<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController; 
use App\Http\Controllers\Backend\EmployeeController; 
use App\Http\Controllers\Backend\CustomerController; 
use App\Http\Controllers\Backend\SupplierController; 
use App\Http\Controllers\Backend\SalaryController;
use App\Http\Controllers\Backend\EmployeeAttendanceController;

 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('index');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';


Route::get('/admin/logout', [AdminController::class, 'AdminDestroy'])->name('admin.logout');

Route::get('/admin/logout/page', [AdminController::class, 'AdminLogoutPage'])->name('admin.logout.page'); 

Route::get('/admin/profile', [AdminController::class, 'AdminProfile'])->name('admin.profile'); 

Route::post('/admin/profile/store', [AdminController::class, 'AdminProfileStore'])->name('admin.profile.store'); 

Route::middleware(['auth'])->group(function() {


Route::get('/admin/change/password', [AdminController::class, 'AdminChangePassword'])->name('admin.change.password'); 

Route::post('/admin/update/password', [AdminController::class, 'AdminUpdatePassword'])->name('admin.update.password'); 


// Employee all Route
Route::controller(EmployeeController::class)->group(function(){

    Route::get('/all/employee','AllEmployee')->name('all.employee');
    Route::get('/add/employee','AddEmployee')->name('add.employee');
    Route::post('/store/employee','StoreEmployee')->name('employee.store'); 
    Route::get('/edit/employee/{id}','EditEmployee')->name('edit.employee');
    Route::post('/update/employee','UpdateEmployee')->name('employee.update');
    Route::get('/delete/employee/{id}','DeleteEmployee')->name('delete.employee');

});


// Customer all Route
Route::controller(CustomerController::class)->group(function(){

    Route::get('/all/customer','AllCustomer')->name('all.customer');
    Route::get('/add/customer','AddCustomer')->name('add.customer');
    Route::post('/store/customer','StoreCustomer')->name('customer.store'); 
    Route::get('/edit/customer/{id}','EditCustomer')->name('edit.customer');
    Route::post('/update/customer','UpdateCustomer')->name('customer.update');
    Route::get('/delete/customer/{id}','DeleteCustomer')->name('delete.customer');

});


// Supplier all Route
Route::controller(SupplierController::class)->group(function(){

    Route::get('/all/supplier','AllSupplier')->name('all.supplier');
    Route::get('/add/supplier','AddSupplier')->name('add.supplier');
    Route::post('/store/supplier','StoreSupplier')->name('supplier.store'); 
    Route::get('/edit/supplier/{id}','EditSupplier')->name('edit.supplier');
    Route::post('/update/supplier','UpdateSupplier')->name('supplier.update');
    Route::get('/delete/supplier/{id}','DeleteSupplier')->name('delete.supplier');

    Route::get('/details/supplier/{id}','DetailsSupplier')->name('details.supplier');

});


// Salary all Route
Route::controller(SalaryController::class)->group(function(){

    Route::get('/add/advance/salary','AddAdvanceSalary')->name('add.advance.salary');
    Route::post('/advance/salary/store','AdvanceSalaryStore')->name('advance.salary.store');
    Route::get('/all/advance/salary','AllAdvanceSalary')->name('all.advance.salary');

    Route::get('/pay/salary','PaySalary')->name('pay.salary');
    Route::get('/pay/now/salary/{id}','payNowSalary')->name('pay.now.salary');
    Route::get('/all/paid/salary','paidSalary')->name('all.paid.salary');

    Route::post('/paid/due/amount','dueStoreAmount')->name('due.store.amount');
    Route::get('/salary/history/{id}', 'salaryHistory')->name('salary_history');
    Route::get('/salary/destroy/{id}', 'destroyPaidSalary')->name('destroy_paid_salary');
});

//Attendance
Route::controller(EmployeeAttendanceController::class)->group(function () {
    Route::get('/attend/view', 'view')->name('employee-attend-view');
    Route::get('/attend/add', 'add')->name('employee-attend-add');
    Route::post('/attend/store', 'store')->name('employee-attend-store');
    Route::get('/attend/edit/{date}', 'edit')->name('employee-attend-edit');
    Route::post('/attend/update/{id}', 'update')->name('employee-attend-update');
    Route::get('/attent/details/{date}','details')->name('employee.attend.details');
});

//Attendance 2
Route::controller(EmployeeAttendanceController::class)->group(function () {
    Route::get('/view', 'index')->name('view_attend');
    Route::get('/add', 'create')->name('add_attend');
    Route::post('/store', 'storeAtend')->name('store_attend');
    Route::get('/show/{id}', 'show')->name('show_attend');
    Route::get('/edit/{id}', 'edit')->name('edit_attend');
    Route::post('/update/{id}', 'update')->name('update_attend');
    Route::get('/destroy/{id}', 'destroy')->name('destroy_attend');
});




}); // End User Middleware 