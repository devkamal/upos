-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 21, 2022 at 06:12 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tpos`
--

-- --------------------------------------------------------

--
-- Table structure for table `amployee_attendances`
--

CREATE TABLE `amployee_attendances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` int(11) NOT NULL COMMENT 'employee_id=user_id',
  `date` date NOT NULL,
  `attend_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `amployee_attendances`
--

INSERT INTO `amployee_attendances` (`id`, `employee_id`, `date`, `attend_status`, `created_at`, `updated_at`) VALUES
(1, 1, '1970-01-01', 'present', '2022-12-20 09:30:01', '2022-12-20 09:30:01'),
(2, 3, '1970-01-01', 'present', '2022-12-20 09:30:01', '2022-12-20 09:30:01'),
(3, 4, '1970-01-01', 'present', '2022-12-20 09:30:01', '2022-12-20 09:30:01'),
(4, 5, '1970-01-01', 'present', '2022-12-20 09:30:01', '2022-12-20 09:30:01'),
(5, 1, '1970-01-01', 'present', '2022-12-20 09:33:37', '2022-12-20 09:33:37'),
(6, 3, '1970-01-01', 'present', '2022-12-20 09:33:37', '2022-12-20 09:33:37'),
(7, 4, '1970-01-01', 'present', '2022-12-20 09:33:37', '2022-12-20 09:33:37'),
(8, 5, '1970-01-01', 'present', '2022-12-20 09:33:37', '2022-12-20 09:33:37'),
(9, 1, '1970-01-01', 'present', '2022-12-20 09:34:52', '2022-12-20 09:34:52'),
(10, 3, '1970-01-01', 'present', '2022-12-20 09:34:52', '2022-12-20 09:34:52'),
(11, 4, '1970-01-01', 'present', '2022-12-20 09:34:52', '2022-12-20 09:34:52'),
(12, 5, '1970-01-01', 'present', '2022-12-20 09:34:52', '2022-12-20 09:34:52'),
(13, 1, '1970-01-01', 'present', '2022-12-20 09:50:31', '2022-12-20 09:50:31'),
(14, 3, '1970-01-01', 'present', '2022-12-20 09:50:31', '2022-12-20 09:50:31'),
(15, 4, '1970-01-01', 'present', '2022-12-20 09:50:31', '2022-12-20 09:50:31'),
(16, 5, '1970-01-01', 'present', '2022-12-20 09:50:31', '2022-12-20 09:50:31'),
(17, 1, '1970-01-01', 'present', '2022-12-20 09:51:17', '2022-12-20 09:51:17'),
(18, 3, '1970-01-01', 'present', '2022-12-20 09:51:17', '2022-12-20 09:51:17'),
(19, 4, '1970-01-01', 'present', '2022-12-20 09:51:17', '2022-12-20 09:51:17'),
(20, 5, '1970-01-01', 'present', '2022-12-20 09:51:17', '2022-12-20 09:51:17'),
(21, 1, '1970-01-01', 'present', '2022-12-20 09:54:00', '2022-12-20 09:54:00'),
(22, 3, '1970-01-01', 'present', '2022-12-20 09:54:00', '2022-12-20 09:54:00'),
(23, 4, '1970-01-01', 'present', '2022-12-20 09:54:00', '2022-12-20 09:54:00'),
(24, 5, '1970-01-01', 'present', '2022-12-20 09:54:00', '2022-12-20 09:54:00'),
(25, 1, '1970-01-01', 'present', '2022-12-20 09:54:25', '2022-12-20 09:54:25'),
(26, 3, '1970-01-01', 'present', '2022-12-20 09:54:25', '2022-12-20 09:54:25'),
(27, 4, '1970-01-01', 'present', '2022-12-20 09:54:25', '2022-12-20 09:54:25'),
(28, 5, '1970-01-01', 'present', '2022-12-20 09:54:25', '2022-12-20 09:54:25'),
(29, 1, '1970-01-01', 'present', '2022-12-20 09:54:44', '2022-12-20 09:54:44'),
(30, 3, '1970-01-01', 'present', '2022-12-20 09:54:44', '2022-12-20 09:54:44'),
(31, 4, '1970-01-01', 'present', '2022-12-20 09:54:44', '2022-12-20 09:54:44'),
(32, 5, '1970-01-01', 'present', '2022-12-20 09:54:44', '2022-12-20 09:54:44'),
(33, 1, '1970-01-01', 'present', '2022-12-20 10:03:30', '2022-12-20 10:03:30'),
(34, 3, '1970-01-01', 'present', '2022-12-20 10:03:30', '2022-12-20 10:03:30'),
(35, 4, '1970-01-01', 'present', '2022-12-20 10:03:30', '2022-12-20 10:03:30'),
(36, 5, '1970-01-01', 'present', '2022-12-20 10:03:30', '2022-12-20 10:03:30'),
(37, 1, '2022-12-20', 'present', '2022-12-20 10:07:58', '2022-12-20 10:07:58'),
(38, 3, '2022-12-20', 'present', '2022-12-20 10:07:58', '2022-12-20 10:07:58'),
(39, 4, '2022-12-20', 'present', '2022-12-20 10:07:58', '2022-12-20 10:07:58'),
(40, 5, '2022-12-20', 'present', '2022-12-20 10:07:58', '2022-12-20 10:07:58'),
(41, 1, '1970-01-01', 'Leave', '2022-12-20 10:23:50', '2022-12-20 10:23:50'),
(42, 3, '1970-01-01', 'present', '2022-12-20 10:23:50', '2022-12-20 10:23:50'),
(43, 4, '1970-01-01', 'Leave', '2022-12-20 10:23:50', '2022-12-20 10:23:50'),
(44, 5, '1970-01-01', 'Absent', '2022-12-20 10:23:50', '2022-12-20 10:23:50');

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `checkin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `checkout` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=Present , 1=Late',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendances`
--

INSERT INTO `attendances` (`id`, `employee_id`, `date`, `checkin`, `checkout`, `note`, `created_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 4, '2022-12-21', '10.00 am', '12.00pm', 'this is employee attendance system', 1, 1, NULL, NULL),
(2, 1, '2022-12-01', '11am', '6 pm', 'This is note........', 1, 1, NULL, NULL),
(3, 5, '2022-12-10', '5am', '8pm', 'laksjdflkasjdhflkasdjfh', 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shopname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_holder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_branch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `phone`, `address`, `shopname`, `image`, `account_holder`, `account_number`, `bank_name`, `bank_branch`, `city`, `created_at`, `updated_at`) VALUES
(1, 'customer', 'customer1@gmail.com', '32423233', 'India', 'easyshope', 'upload/customer/1751768075362894.png', 'kazi ariyan', '2323232323', 'brack', 'Paltan', 'Dhaka', '2022-12-10 02:04:18', '2022-12-10 02:04:18');

-- --------------------------------------------------------

--
-- Table structure for table `due_salaries`
--

CREATE TABLE `due_salaries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `due_salaries`
--

INSERT INTO `due_salaries` (`id`, `amount`, `created_at`, `updated_at`) VALUES
(3, '30000', NULL, NULL),
(4, '30000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vacation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `email`, `phone`, `address`, `experience`, `image`, `salary`, `vacation`, `city`, `created_at`, `updated_at`) VALUES
(1, 'Kazi Ariyan', 'kazi@gmail.com', '23232323', 'bangladesh', '3 Year', 'upload/employee/1751724717339966.jpg', '30000', '2', 'Dhaka', '2022-12-09 02:34:32', NULL),
(3, 'Jashim', 'jashim@gmail.com', '23232323', 'iadsfasdfas', '3 Year', 'upload/employee/1752549968053331.jpg', '30000', '3', 'Dhaka', '2022-12-18 05:11:33', NULL),
(4, 'kamal hossen', 'kamal@gmail.com', '01913556236', 'Narayanganj, Dhaka, Bangladesh', '3 Year', 'upload/employee/1752702234487384.jpg', '35000', '10', 'Dhaka', '2022-12-19 21:31:45', NULL),
(5, 'santho', 'santho@gmail.com', '01236455250', 'Bhulta, Rupganj, Narayanganj, Dhaka, Bangladesh', '2 Year', 'upload/employee/1752720063555695.jpeg', '50000', '2', 'Dhaka', '2022-12-20 02:15:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_12_09_074103_create_employees_table', 2),
(6, '2022_12_09_155039_create_customers_table', 3),
(7, '2022_12_10_080651_create_suppliers_table', 4),
(8, '2022_12_18_102630_create_salaries_table', 5),
(9, '2022_12_18_135542_create_pay_salaries_table', 6),
(10, '2022_12_20_134559_create_amployee_attendances_table', 7),
(11, '2022_12_21_114608_create_due_salaries_table', 8),
(15, '2022_12_21_142042_create_attendances_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pay_salaries`
--

CREATE TABLE `pay_salaries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` int(11) NOT NULL,
  `salary_month` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paid_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salaries`
--

CREATE TABLE `salaries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` int(11) NOT NULL,
  `month` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advanced_salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `salaries`
--

INSERT INTO `salaries` (`id`, `employee_id`, `month`, `year`, `advanced_salary`, `created_at`, `updated_at`) VALUES
(1, 1, 'December', '2022', '10000', '2022-12-17 20:55:44', NULL),
(2, 1, 'January', '2023', '5000', '2022-12-18 05:22:41', NULL),
(3, 3, 'January', '2023', '10000', '2022-12-18 05:23:09', NULL),
(4, 3, 'November', '2022', '2000', '2022-12-18 15:15:08', NULL),
(6, 4, 'June', '2023', '1520', '2022-12-20 02:12:57', NULL),
(7, 4, 'February', '2024', '50000', '2022-12-20 02:13:31', NULL),
(8, 5, 'August', '2023', '1250', '2022-12-20 02:15:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_holder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_branch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `email`, `phone`, `address`, `type`, `image`, `shop`, `account_holder`, `account_number`, `bank_name`, `bank_branch`, `city`, `created_at`, `updated_at`) VALUES
(1, 'Raju', 'raju@gmail.com', '2323232323', 'bangladesh', 'Whole Seller', 'upload/supplier/1751815338446140.jpg', 'raihan', 'raju hassan', '4343434', 'ucb', 'paltan', 'Dhaka', '2022-12-10 02:34:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `photo`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '01455555', '202212081050businessman-icon-vector-male-avatar-profile-image-profile-businessman-icon-vector-male-avatar-profile-image-182095609.jpg', NULL, '$2y$10$KJt1Ml5az7hEcPWUR.kcA.IGvLcgiKpiWdWr4PzhQPUwtzknfXV6K', NULL, '2022-12-04 04:55:22', '2022-12-08 04:50:05'),
(4, 'Ariyan1', 'ariyan@gmail.com', '01711223355', '2022120810310bfc3c5b20c439c4972383592e1c26bc.jpg', NULL, '$2y$10$8fmElAlItYQdJXSyRiw3uuxOgPYzYpyF8PS8093lbYHlSS2Vt3V3W', NULL, '2022-12-04 04:59:29', '2022-12-08 04:33:27'),
(5, 'raju', 'raju@gmail.com', '01711223344', NULL, NULL, '$2y$10$3sZNA3U3wILrlXRwR/SH.uq.Xw9WBI4DSmqaZEyv7PZfGj1VGQyn6', NULL, '2022-12-04 05:00:54', '2022-12-04 05:00:54'),
(6, 'khan', 'khan@gmail.com', '01711', NULL, NULL, '$2y$10$1AbUoXT/nfr3q8TsoLpnhOVWoLaPGL9RcAGtu982vCuh0xFFG33TW', NULL, '2022-12-04 05:28:07', '2022-12-04 05:28:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `amployee_attendances`
--
ALTER TABLE `amployee_attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `due_salaries`
--
ALTER TABLE `due_salaries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pay_salaries`
--
ALTER TABLE `pay_salaries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `salaries`
--
ALTER TABLE `salaries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `amployee_attendances`
--
ALTER TABLE `amployee_attendances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `due_salaries`
--
ALTER TABLE `due_salaries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pay_salaries`
--
ALTER TABLE `pay_salaries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salaries`
--
ALTER TABLE `salaries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
