@extends('admin_dashboard')
@section('admin')
<div class="content">
   <!-- Start Content-->
   <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                  <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addAttendanceEmployee">
                  <i class="fa fa-plus-circle"></i> Add Attendance Employee
            </button>
                
                  </ol>
               </div>
               <h4 class="page-title">All Employee Attendance List 2...</h4>
            </div>
         </div>
      </div>
      <!-- end page title --> 
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <h4 class="header-title">All Employee</h4>
                  <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                     <thead>
                        <tr>
                        <th>SL.</th>
                        <th>Date</th>
                        <th>Employee</th>
                        <th>CheckIn</th>
                        <th>CheckOut</th>
                        <th>Status</th>
                        <th>Created By</th>
                        <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($allData as $key => $item)
                           <tr>
                              <td>{{$key+1}}</td>
                              <td>{{date('Y-m-d',strtotime($item->date))}}</td>
                              <td>{{$item['employee']['name']}}</td>
                              <td>{{$item->checkin}}</td>
                              <td>{{$item->checkout}}</td>
                              <td>
                                    @if ($item->status == 0)
                                          <span style="color:red">Late</span>
                                    @else
                                          <span style="color:green">Present</span>
                                    @endif
                              </td>
                              <td> {{ Auth::user()->name  }}</td>
                              <td>
                              <a title="Edit" id="delete" href="{{ route('destroy_attend',$item->id) }}" class="btn btn-sm btn-danger">
                                    <i class="fa fa-trash"></i>
                              </a>
                              </td>
                          </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
      <!-- end row-->
   </div>
   <!-- container -->
</div>
<!-- content -->

<!-- Add attendance Employee Modal -->
<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="addAttendanceEmployee" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel">Add Attendance</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         </div>
         <form action="{{ route('store_attend') }}" method="post">
            @csrf
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-6">
                     <label for="date">Date:</label>
                     <input type="date" name="date" class="form-control">
                     @error('date')
                           <span style="color:red;font-size:13px;">{{ $message }}</span>
                     @enderror
                  </div>
                  <div class="col-md-6">
                     <label for="employee_id">Employee:</label>
                     <select name="employee_id" id="employee_id" class="form-control">
                        <option value="">Select Employee</option>
                        @foreach ($employees as $item)
                              <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                     </select>
                     @error('employee_id')
                           <span style="color:red;font-size:13px;">{{ $message }}</span>
                     @enderror
                  </div>
               </div>
               <div class="row mt-2">
                  <div class="col-md-6">
                     <label for="checkin">CheckIn:</label>
                     <input type="checkin" name="checkin" class="form-control">
                     @error('checkin')
                           <span style="color:red;font-size:13px;">{{ $message }}</span>
                     @enderror
                  </div>
                  <div class="col-md-6">
                     <label for="checkout">CheckOut:</label>
                     <input type="checkout" name="checkout" class="form-control">
                     @error('checkout')
                           <span style="color:red;font-size:13px;">{{ $message }}</span>
                     @enderror
                  </div>
               </div>

               <div class="row mt-2">
                  <div class="col-md-12">
                     <label for="note">Note:</label>
                     <textarea name="note" id="note" class="form-control"></textarea>
                     @error('note')
                           <span style="color:red;font-size:13px;">{{ $message }}</span>
                     @enderror
                  </div>
               </div>

            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-primary">Submit</button>
            </div>
         </form>
      </div>
   </div>
</div>


@endsection