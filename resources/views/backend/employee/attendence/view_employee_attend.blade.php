@extends('admin_dashboard')
@section('admin')
<div class="content">
   <!-- Start Content-->
   <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                  <a href="{{ route('employee-attend-add') }}" class="btn btn-primary float-sm-right"> <i class="fa fa-plus-circle"></i> Add Employee Attendance</a>
                  </ol>
               </div>
               <h4 class="page-title">All Employee Attendance List...</h4>
            </div>
         </div>
      </div>
      <!-- end page title --> 
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <h4 class="header-title">All Employee</h4>
                  <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                     <thead>
                        <tr>
                        <th>SL.</th>
                        <th>Date</th>
                        <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($allData as $key => $item)
                           <tr>
                              <td>{{$key+1}}</td>
                              <td>{{date('Y-m-d',strtotime($item->date))}}</td>
                              <td>
                              <a title="Edit" href="{{ route('employee-attend-edit',$item->date) }}" class="btn btn-sm btn-primary">
                                    <i class="fa fa-edit"></i>
                              </a>
                              <a title="Details" href="{{ route('employee.attend.details',$item->date) }}" class="btn btn-sm btn-info">
                                    <i class="fa fa-eye"></i>
                              </a>
                              </td>
                          </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
      <!-- end row-->
   </div>
   <!-- container -->
</div>
<!-- content -->



@endsection