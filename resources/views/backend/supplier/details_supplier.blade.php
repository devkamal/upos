@extends('admin_dashboard')
@section('admin')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>



  <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                                            
                                            <li class="breadcrumb-item active">Edit Supplier</li>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">Edit Supplier</h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title -->

<div class="row">
 

        <div class="col-lg-8 col-xl-12">
                                <div class="card">
                                    <div class="card-body">
                                       
                                            
<div class="tab-pane" id="settings">
     <form method="post" action="{{ route('supplier.update') }}" enctype="multipart/form-data"  >
        @csrf
 
        

        <h5 class="mb-4 text-uppercase"><i class="mdi mdi-account-circle me-1"></i> Supplier Details </h5>
        <div class="row">


             <div class="col-md-6">
        <div class="mb-3">
            <label for="name" class="form-label">Supplier Name </label>
            <p class="text-danger">{{ $supplier->name }}</p>
        </div>
    </div>
    


     <div class="col-md-6">
         <div class="mb-3">
            <label for="name" class="form-label">Supplier Email </label>
            <p class="text-danger">{{ $supplier->email }}</p>
        </div>
    </div> <!-- end col -->


     <div class="col-md-6">
         <div class="mb-3">
            <label for="name" class="form-label">Supplier Phone </label>
          <p class="text-danger">{{ $supplier->phone }}</p>
        </div>
    </div> <!-- end col -->


     <div class="col-md-6">
         <div class="mb-3">
            <label for="name" class="form-label">Supplier address </label>
          <p class="text-danger">{{ $supplier->address }}</p>
        </div>
    </div> <!-- end col -->



      <div class="col-md-6">
         <div class="mb-3">
            <label for="name" class="form-label">Supplier Shop Name </label>
            <p class="text-danger">{{ $supplier->shop }}</p>
        </div>
    </div> <!-- end col -->


 


     <div class="col-md-6">
         <div class="mb-3">
            <label for="name" class="form-label">account_holder </label>
            <p class="text-danger">{{ $supplier->account_holder }}</p>
        </div>
    </div> <!-- end col -->



     <div class="col-md-6">
         <div class="mb-3">
            <label for="name" class="form-label">account_number </label>
            <p class="text-danger">{{ $supplier->account_number }}</p>
        </div>
    </div> <!-- end col -->

     <div class="col-md-6">
         <div class="mb-3">
            <label for="name" class="form-label">bank_name </label>
            <p class="text-danger">{{ $supplier->bank_name }}</p>
        </div>
    </div> <!-- end col -->

     <div class="col-md-6">
         <div class="mb-3">
            <label for="name" class="form-label">bank_branch </label>
            <p class="text-danger">{{ $supplier->bank_branch }}</p>
        </div>
    </div> <!-- end col -->
   


     <div class="col-md-6">
         <div class="mb-3">
            <label for="name" class="form-label">city </label>
           <p class="text-danger">{{ $supplier->city }}</p>
        </div>
    </div> <!-- end col -->


    <div class="col-md-6">
         <div class="mb-3">
            <label for="experience" class="form-label">Supplier Type </label>
          <p class="text-danger">{{ $supplier->type }}</p>

        </div>
    </div> <!-- end col -->

   

 
 


              <div class="mb-3">
                <label for="example-fileinput" class="form-label"> Supplier Image </label>

                 <img id="showImage" src="{{ asset($supplier->image) }} " class="rounded-circle avatar-lg img-thumbnail" alt="profile-image">
            </div>
            


        </div> <!-- end row -->

 

        
        
        
        <div class="text-end">
            <button type="submit" class="btn btn-success waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Save Changes </button>
        </div>
    </form>
</div>
                                            <!-- end settings content-->
    
                                         
                                    </div>
                                </div> <!-- end card-->

                            </div> <!-- end col -->
                        </div>
                        <!-- end row-->

                    </div> <!-- container -->

                </div> <!-- content -->



 

@endsection