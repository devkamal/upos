@extends('admin_dashboard')
@section('admin')
<div class="content">
   <!-- Start Content-->
   <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <a href="{{ route('pay.salary') }}" class="btn btn-blue waves-effect waves-light">Back</a>
                  </ol>
               </div>
               <h4 class="page-title">All Pay Salary</h4>
            </div>
         </div>
      </div>
      <!-- end page title --> 
      <div class="row">
         <div class="col-12">
            <div class="card">

               <div class="card-body">
                  <h4 class="header-title">{{ date("F Y") }}</h4>
                  <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                     <thead>
                        <tr>
                           <th>Image</th>
                           <th>Name</th>
                           <th>Month</th>
                           <th>Salary</th>
                           <th>Advance</th>
                           <th>Due</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                       
                  
                           <tr>
                              <td><img src="{{ asset($paysalaries->image) }}" style="width:50px; height: 40px;"></td>
                              <td>{{ $paysalaries->name }}</td>
                              <td><span class="badge bg-info">{{ date("F", strtotime('-1 month')) }}</span> </td>
                              <td> {{ $paysalaries->salary }}</td>
                              <td>{{ $paysalaries['advance']['advanced_salary'] }}</td>

                           <form action="{{ route('due.store.amount') }}" method="post">
                           @csrf
                              <td>
                                 @if($paysalaries->advanced_salary == !NULL)
                                 <span>No Salary</span>
                                 @else
                                 @php 
                                    $amount = $paysalaries->salary - $paysalaries['advance']['advanced_salary'];
                                 @endphp 
                                    <input type="text" name="amount" style="color:#000" value="{{ round($amount) }}">
                                 @endif
                              </td>
                              <td><button class="btn btn-danger rounded-pill waves-effect waves-light" type="submit">Pay Now</button></td>
                              </form>
                             
                           </tr>
                          
                     </tbody>
                  </table>
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
      <!-- end row-->
   </div>
   <!-- container -->
</div>
<!-- content -->
@endsection