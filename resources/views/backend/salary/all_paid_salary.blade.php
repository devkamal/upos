@extends('admin_dashboard')
@section('admin')
<div class="content">
   <!-- Start Content-->
   <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title">All Paid Salary</h4>
            </div>
         </div>
      </div>
      <!-- end page title --> 
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <h4 class="header-title">All Paid Salary</h4>
                  <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                     <thead>
                        <tr>
                           <th>Sl</th>
                           <th>Image</th>
                           <th>Name</th>
                           <th>Month</th>
                           <th>Salary</th>
                           <th>Advance</th>
                           <th>Paid</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                     @foreach($paids as $key=> $item)
                        <tr>
                           <td>{{ $key+1 }}</td>
                           <td><img src="{{ asset($item->image) }}" style="width:50px; height: 40px;"></td>
                           <td>{{ $item->name }}</td>
                           <td><span class="badge bg-info">{{ date("F", strtotime('-1 month')) }}</span> </td>
                           <td> {{ $item->salary }}</td>
                           <td>{{ $item['advance']['advanced_salary'] }}</td>

                           <td>
                              @if($item->advanced_salary == NULL)
                              <span>Paid Salary</span>
                              @else
                              @php 
                                 $amount = $item->salary - $item['advance']['advanced_salary'];
                              @endphp 
                                 <strong style="color:#fff"> {{ round($amount) }}</strong>
                              @endif
                           </td>

                           <td> 
                              <a href="{{ route('salary_history',$item->id) }}" class="btn btn-primary rounded-pill waves-effect waves-light btn-sm" >History</a>
                              <a id="delete" href="{{ route('destroy_paid_salary',$item->id) }}" class="btn btn-danger rounded-pill waves-effect waves-light btn-sm">Remove</a>
                           </td>
                        </tr>
                        @endforeach
                        
                     </tbody>
                  </table>
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
      <!-- end row-->
   </div>
   <!-- container -->
</div>
<!-- content -->
@endsection