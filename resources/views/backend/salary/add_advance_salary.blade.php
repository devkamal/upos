@extends('admin_dashboard')
@section('admin')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>



  <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                                            
                                            <li class="breadcrumb-item active">Add Advance Salary </li>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">Add Advance Salary</h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title -->

<div class="row">
 

        <div class="col-lg-8 col-xl-8">
                                <div class="card">
                                    <div class="card-body">
                                       
                                            
<div class="tab-pane" id="settings">
     <form method="post" action="{{ route('advance.salary.store') }}" >
        @csrf
 


        <h5 class="mb-4 text-uppercase"><i class="mdi mdi-account-circle me-1"></i>Add Advance Salary </h5>
        <div class="row">


      
     <div class="col-md-12">
         <div class="mb-3">
            <label for="experience" class="form-label">Employee Name </label>
           <select name="employee_id" class="form-select" id="example-select">
             <option selected="" >Select Employe  </option>
             @foreach($employee as $item)
                <option value="{{ $item->id }}">{{ $item->name }} </option>
            @endforeach
            </select>
        </div>
    </div> <!-- end col -->
 
  

     <div class="col-md-12">
         <div class="mb-3">
            <label for="experience" class="form-label">Salary Month </label>
           <select name="month" class="form-select" id="example-select">
             <option selected="" >Select Month  </option>
                <option value="January">January </option>
                <option value="February">February </option>
                <option value="March">March </option>
                <option value="April">April </option>
                <option value="May">May </option>
                <option value="June">June </option>
                <option value="July">July </option>
                <option value="August">August </option>
                <option value="September">September </option>
                <option value="October">October </option>
                <option value="November">November </option>
                <option value="December">December </option>

            </select>
        </div>
    </div> <!-- end col -->


      <div class="col-md-12">
         <div class="mb-3">
            <label for="experience" class="form-label">Salary Year </label>
           <select name="year" class="form-select" id="example-select">
             <option selected="" >Select Year  </option>
                <option value="2022">2022 </option>
                <option value="2023">2023 </option>
                <option value="2024">2024 </option>
                <option value="2025">2025 </option>
                

            </select>
        </div>
    </div> <!-- end col -->

    

    <div class="col-md-12">
         <div class="mb-3">
            <label for="name" class="form-label">Advamce Sallary </label>
            <input type="text" name="advanced_salary" class="form-control @error('advanced_salary') is-invalid @enderror" >
            @error('advanced_salary')
            <span class="text-danger">{{ $message }}</span>
            @enderror 
        </div>
    </div> <!-- end col -->

  

 



        </div> <!-- end row --> 
        
        
        <div class="text-end">
            <button type="submit" class="btn btn-success waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Save Changes </button>
        </div>
    </form>
</div>
                                            <!-- end settings content-->
    
                                         
                                    </div>
                                </div> <!-- end card-->

                            </div> <!-- end col -->
                        </div>
                        <!-- end row-->

                    </div> <!-- container -->

                </div> <!-- content -->


 

@endsection