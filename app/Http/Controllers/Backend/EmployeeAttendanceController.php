<?php

namespace App\Http\Controllers\Backend;

use App\Models\Employee;
use App\Models\Attendance;
use Illuminate\Http\Request;
use App\Models\AmployeeAttendance;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class EmployeeAttendanceController extends Controller
{
    public function view(){
        $allData = AmployeeAttendance::select('date')->groupBy('date')->orderBy('id','desc')->get();
        return view('backend.employee.attendence.view_employee_attend',compact('allData'));
    }

    public function add(){
        $employees = Employee::all();
        return view('backend.employee.attendence.add_employee_attend',compact('employees'));
    }

    public function store(Request $request){
        AmployeeAttendance::where('date',date('Y-m-d',strtotime($request->date)))->delete();
        $countemployee = count($request->employee_id);
        for($i=0; $i <$countemployee ; $i++){
            $attend_status = 'attend_status'.$i;
            $attend = new AmployeeAttendance();
            $attend->date = date('Y-m-d',strtotime($request->date));
            $attend->employee_id   = $request->employee_id[$i];
            $attend->attend_status = $request->$attend_status;
            $attend->save();
        }
        return redirect()->route('employee-attend-view')->with('success','Data Inserted successfully!');
    }

    public function edit($date){
       $editData = AmployeeAttendance::where('date',$date)->get();
       $employees = Employee::all();
       return view('backend.employee.attendence.add_employee_attend',compact('editData','employees'));
    }

    public function details($date){
        $details = AmployeeAttendance::where('date',$date)->get();
        return view('backend.employee.attendence.details_employee_attend',compact('details'));
    }








    /*******Attendance Two 2******/
    public function index(){
        $employees = Employee::all();
        $allData = Attendance::orderBy('id','desc')->get();
        return view('backend.employee.attendence.view_emp_attend',compact('allData','employees'));
    }

    public function storeAtend(Request $request){
       // return response()->json($request);

        $validateData = $request->validate([
            'date' => 'required',
            'checkin' => 'required',
            'checkout' => 'required',
            'note' => 'required',
        ]);

        Attendance::insert([
            'date'      => date('Y-m-d',strtotime($request->date)),
            'checkin'   => $request->checkin,
            'checkout'  => $request->checkout,
            'note'      => $request->note,
            'employee_id' => $request->employee_id,
            'created_by'  => Auth::user()->id,
            'status'      => 1,
        ]);

        $notification = array(
            'message' => 'Employee Attendance Add Successfully',
            'alert-type' => 'success'

        );
        return redirect()->route('view_attend')->with($notification);

    }

    public function destroy($id){
        Attendance::where('id',$id)->delete();

        $notification = array(
            'message' => 'Employee Attendance delete Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    
}
