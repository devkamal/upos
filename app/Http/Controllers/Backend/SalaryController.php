<?php

namespace App\Http\Controllers\Backend;

use Carbon\Carbon; 
use App\Models\Salary;
use App\Models\Employee;
use App\Models\DueSalary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SalaryController extends Controller
{
    

    public function AddAdvanceSalary(){
        $employee = Employee::latest()->get();
        return view('backend.salary.add_advance_salary',compact('employee'));
    } // End Method 


    public function AdvanceSalaryStore(Request $request){
        $validatedData = $request->validate([ 
            'month' => 'required|max:255',
            'advanced_salary' => 'required',
            'year' => 'required', 
        ]);

        $month = $request->month;
        $employee_id = $request->employee_id;

        $advanced = Salary::where('month',$month)->where('employee_id',$employee_id)->first();

        if ($advanced === NULL) {

           Salary::insert([
            'employee_id' => $request->employee_id,
            'month' => $request->month,
            'year' => $request->year,
            'advanced_salary' => $request->advanced_salary,
            'created_at' => Carbon::now(),  

        ]); 

         $notification = array(
            'message' => 'Advance Salary Paid Successfully',
            'alert-type' => 'success'

        );

        return redirect()->back()->with($notification);

        }else{

             $notification = array(
            'message' => 'Advance Already Paid Successfully',
            'alert-type' => 'warning'

        );

        return redirect()->back()->with($notification);

        } 

    }// End Method 

    public function AllAdvanceSalary(){
        $salary = Salary::latest()->get();
        return view('backend.salary.all_advance_salary',compact('salary'));
    }// End Method 


    public function PaySalary(){
        $employee = Employee::latest()->get();
        return view('backend.salary.pay_salary',compact('employee'));
    }// End Method 

    public function payNowSalary($id){
        $paysalaries = Employee::findOrFail($id);
        //return response()->json($paysalaries);
        return view('backend.salary.paid_salary',compact('paysalaries'));
    }

    public function paidSalary(){
        $paids = Employee::latest()->get();
        return view('backend.salary.all_paid_salary',compact('paids'));
    }//End Method

    public function dueStoreAmount(Request $request){
        //return response()->json($request);

        $dueamounts = DueSalary::insert([
            'amount' => $request->amount,
        ]);

        $notification = array(
            'message' => 'Full paid of last month salary.',
            'alert-type' => 'success',
        );
        return redirect()->route('all.paid.salary')->with($notification);
    }

    public function salaryHistory($id){
        return response()->json($id);
    }

    public function destroyPaidSalary($id){
        return response()->json($id);
    }

}
 