<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Supplier;
use Carbon\Carbon; 
use Intervention\Image\Facades\Image;

class SupplierController extends Controller
{
     public function AllSupplier(){

        $supplier = Supplier::latest()->get();
        return view('backend.supplier.all_supplier',compact('supplier'));

    } // End Method 


     public function AddSupplier(){
 
        return view('backend.supplier.add_supplier');

    } // End Method 

     public function StoreSupplier(Request $request){

         

        $image = $request->file('image');
        $name_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(300,300)->save('upload/supplier/'.$name_gen);
        $save_url = 'upload/supplier/'.$name_gen;

        Supplier::insert([

            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address, 
            'shop' => $request->shop,
            'account_holder' => $request->account_holder,

            'account_number' => $request->account_number,
            'bank_name' => $request->bank_name,
            'bank_branch' => $request->bank_branch,
            'city' => $request->city, 
            'type' => $request->type, 
            'image' => $save_url,
            'created_at' => Carbon::now(),  

        ]);

         $notification = array(
            'message' => 'Supplier Inserted Successfully',
            'alert-type' => 'success'

        );
        return redirect()->route('all.supplier')->with($notification);


    }// End Method


  public function EditSupplier($id){

        $supplier = Supplier::findOrFail($id);
        return view('backend.supplier.edit_supplier',compact('supplier'));

    }// end mehtod 


     public function UpdateSupplier(Request $request){

        $supplier_id = $request->id;

        if ($request->file('image')) {

        $image = $request->file('image');
        $name_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(300,300)->save('upload/supplier/'.$name_gen);
        $save_url = 'upload/supplier/'.$name_gen;

        Supplier::findOrFail($supplier_id)->update([

            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address, 
            'shop' => $request->shop,
            'account_holder' => $request->account_holder,

            'account_number' => $request->account_number,
            'bank_name' => $request->bank_name,
            'bank_branch' => $request->bank_branch,
            'city' => $request->city, 
            'type' => $request->type, 
            'image' => $save_url,
            'created_at' => Carbon::now(),  

        ]);

         $notification = array(
            'message' => 'Supplier Updated with Image Successfully',
            'alert-type' => 'success'

        );
        return redirect()->route('all.supplier')->with($notification);


        }else{

             Supplier::findOrFail($supplier_id)->update([

            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address, 
            'shop' => $request->shop,
            'account_holder' => $request->account_holder,

            'account_number' => $request->account_number,
            'bank_name' => $request->bank_name,
            'bank_branch' => $request->bank_branch,
            'city' => $request->city, 
            'type' => $request->type, 
            'created_at' => Carbon::now(),  

        ]);

         $notification = array(
            'message' => 'Supplier Updated without Image Successfully',
            'alert-type' => 'success'

        );
        return redirect()->route('all.supplier')->with($notification);

        } 

    }// End Method


 public function DeleteSupplier($id){

        $supplier_image = Supplier::findOrFail($id);
        $img = $supplier_image->image;
        unlink($img);

        Supplier::findOrFail($id)->delete();

         $notification = array(
            'message' => 'Supplier Deleted Successfully',
            'alert-type' => 'success'

        );
        return redirect()->back()->with($notification);

    }// End Method

      public function DetailsSupplier($id){

        $supplier = Supplier::findOrFail($id);
        return view('backend.supplier.details_supplier',compact('supplier'));

    }// end mehtod 


}
